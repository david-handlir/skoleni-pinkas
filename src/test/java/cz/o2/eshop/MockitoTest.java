package cz.o2.eshop;

import cz.o2.eshop.dto.ItemDto;
import cz.o2.eshop.entity.Item;
import cz.o2.eshop.exception.EntityNotFoundException;
import cz.o2.eshop.repository.ItemRepository;
import cz.o2.eshop.service.ItemService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 12.1.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MockitoTest {

    private ItemService itemService;

    @Before
    public void setUp() {
        itemService = new ItemService();
    }

    @Test(expected = EntityNotFoundException.class)
    public void test() {
        itemService.setItemRepository(Mockito.mock(ItemRepository.class));
        itemService.setMapperFacade(Mockito.mock(MapperFacade.class));
        itemService.findOne(1);
    }

    @Test
    public void test2() {
        ItemRepository itemRepositoryMock = Mockito.mock(ItemRepository.class);
        Item returnedtem = new Item();
        returnedtem.setId(1);
        Mockito.when(itemRepositoryMock.findOne(Mockito.anyInt()))
                .thenReturn(returnedtem);
        itemService.setItemRepository(itemRepositoryMock);
        itemService.setMapperFacade(new DefaultMapperFactory.Builder().build().getMapperFacade());
        ItemDto itemDto = itemService.findOne(1);
        Assertions.assertThat(itemDto.getId()).isEqualTo(1);
    }
}
