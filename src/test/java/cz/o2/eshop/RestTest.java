package cz.o2.eshop;

import cz.o2.eshop.dto.ItemDto;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 12.1.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestTest {

    @LocalServerPort
    private int port;

    @Ignore
    @Test
    public void test(){
        String string = new RestTemplate().getForObject("http://localhost:" + port + "/item", String.class);
    }


    private static HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {
            private static final long serialVersionUID = 1L;

            {
                String auth = username + ":" + password;
                byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
                String authHeader = "Basic " + new String(encodedAuth);
                set("Authorization", authHeader);
            }
        };
    }

    @Ignore // works only with base security
    @Test
    public void testRest() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ItemDto[]> responseEntity = restTemplate.exchange("http://localhost:" + port + "/item", HttpMethod.GET,
                new HttpEntity<>(createHeaders("jirka", "jirka")), ItemDto[].class);
        assertThat(responseEntity.getBody().length).isGreaterThan(0);
    }

}
