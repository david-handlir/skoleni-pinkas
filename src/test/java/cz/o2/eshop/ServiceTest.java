package cz.o2.eshop;

import cz.o2.eshop.dto.ItemDto;
import cz.o2.eshop.service.ItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import static org.assertj.core.api.Assertions.*;


@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {

	@Autowired
	private ItemService itemService;

	@Test
	public void itemServiceSaveTest() {
		int size = itemService.findAll("asc", "id").size();
		ItemDto itemDto = new ItemDto();
		itemService.save(itemDto);
		// because of caching in item service
		itemService.clearCache();
		assertThat(itemService.findAll("asc", "id").size())
				.isEqualTo(size + 1);
	}

}
