package cz.o2.eshop;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 12.1.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SeleniumTest {

    @LocalServerPort
    private int port;

    private WebDriver driver;

    @Before
    public void setUp() {
        FirefoxBinary binary = null;
        binary = new FirefoxBinary(new File("d:\\Firefox 47\\firefox.exe"));
        FirefoxProfile profile = new FirefoxProfile();
        driver = new FirefoxDriver(binary, profile);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testPageRendered() {
        driver.get("http://localhost:" + port);
        String title = driver.getTitle();
        Assertions.assertThat(title).isEqualTo("Login Page");

        driver.get("http://localhost:" + port + "/login");
        driver.findElement(By.name("username")).sendKeys("jirka");
        driver.findElement(By.name("password")).sendKeys("jirka");
        driver.findElement(By.name("submit")).submit();

        driver.get("http://localhost:" + port);
        driver.findElement(By.tagName("ul"));

//        driver.findElement(By.id("end"));
    }

}

