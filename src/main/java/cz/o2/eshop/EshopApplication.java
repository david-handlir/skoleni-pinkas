package cz.o2.eshop;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync;

//@Configuration
//@ComponentScan
//@EnableAutoConfiguration
@EnableCaching
@EnableAsync
@SpringBootApplication //Toto nahrazuje tridy uvedene vyse
@ImportResource("classpath:security.xml")
public class EshopApplication {

	@Bean
	public MapperFacade mapperFacade() {
		return new DefaultMapperFactory.Builder().build().getMapperFacade();
	}

	public static void main(String[] args) {
		SpringApplication.run(EshopApplication.class, args);
	}
}
