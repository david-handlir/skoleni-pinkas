package cz.o2.eshop.entity;

import javax.persistence.*;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 11.1.2018.
 */
@Entity
@Table(name = "ordereditem")
public class OrderItem {

    @Id
    @Column(name = "ordereditemid")
    private int id;

    private int quantity;

    @ManyToOne
    @JoinColumn(name = "item_id")
    private Item item;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
