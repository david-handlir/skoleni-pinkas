package cz.o2.eshop.controller;

import cz.o2.eshop.exception.EntityNotFoundException;
import cz.o2.eshop.pojo.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 10.1.2018.
 */

@RestControllerAdvice
public class CustomRestControllerAdvice {

//    Nejvice flexibilni zpusob jak handlit vyjimky
//    ResponseEntity = low level poslani cehosi klientovi
    @ExceptionHandler
    public ResponseEntity<Message> handleItemNotFound(EntityNotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new Message(e.getMessage()));
    }

}
