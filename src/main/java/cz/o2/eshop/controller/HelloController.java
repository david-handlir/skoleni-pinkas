package cz.o2.eshop.controller;

import cz.o2.eshop.pojo.Message;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 9.1.2018.
 */

@Api
@RestController
public class HelloController {

    @GetMapping(value = "/hello")
    public Message hello() {
        return new Message("Yabadaba, it works!");
    }

}
