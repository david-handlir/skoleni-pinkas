package cz.o2.eshop.controller;

import cz.o2.eshop.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.IntStream;


/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 10.1.2018.
 */

@RestController
public class EmailController {

    @Autowired
    private EmailService emailService;

    @GetMapping("/sendEmails")
    public void sendEmails() {
        IntStream.range(0,100000)
//        volam asynchronni metodu kam ji nahazim xxx emailu a ta je postupne odesila pres nekolik vlaken
                .forEach(emailService::sendEmail);
    }


}
