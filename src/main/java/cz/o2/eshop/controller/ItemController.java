package cz.o2.eshop.controller;

import cz.o2.eshop.dto.ItemDto;
import cz.o2.eshop.service.ItemService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 9.1.2018.
 */

@Api
@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping
    public List<ItemDto> getItems(@RequestParam(defaultValue = "asc") String direction,
                                  @RequestParam(defaultValue = "id") String orderBy,
                                  @RequestParam(required = false, name = "User-Agent") String userAgent) {
        System.out.println(userAgent);
        return itemService.findAll(direction, orderBy);
    }

    @GetMapping("/{id}")
    public ItemDto getItem(@PathVariable Integer id) {
        return itemService.findOne(id);
    }

    @PostMapping
    public ItemDto save(@RequestBody @Valid ItemDto itemDto) {
        // hodnota 0 vynuti insert
        itemDto.setId(0);
        return itemService.save(itemDto);
    }

    @PutMapping("/{id}")
    public ItemDto update(@PathVariable Integer id, @RequestBody @Valid ItemDto itemDto) {
        itemDto.setId(id);
        return itemService.save(itemDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        itemService.delete(id);
    }

}
