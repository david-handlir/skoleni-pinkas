package cz.o2.eshop.controller;

import cz.o2.eshop.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 10.1.2018.
 */

@Controller
public class IndexController {

    @Autowired
    private ItemService itemService;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("title", "Uvodni H1cka");
        model.addAttribute("items", itemService.findAll("asc", "id"));
        return "index";
    }
}
