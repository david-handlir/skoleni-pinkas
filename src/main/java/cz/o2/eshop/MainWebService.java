package cz.o2.eshop;

import net.webservicex.GlobalWeather;
import net.webservicex.GlobalWeatherSoap;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 12.1.2018.
 */
public class MainWebService {

    public static void main(String[] args) {

        GlobalWeather globalWeather = new GlobalWeather();
        GlobalWeatherSoap ws = globalWeather.getGlobalWeatherSoap();
        String uganda = ws.getCitiesByCountry("Uganda");
        System.out.printf(uganda);

    }
}
