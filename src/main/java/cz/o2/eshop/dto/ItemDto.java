package cz.o2.eshop.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 10.1.2018.
 */
public class ItemDto {

    private int id;

    @NotNull
    @Size(min = 1)
    private String name;

    @Min(1)
    private int price;
    private int priceEur;
    private int priceUsd;

    // aby se premapovalo musi se jmenovat stejne jako v entite
    private List<OrderItemDto> orderedItems;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPriceEur() {
        return priceEur;
    }

    public void setPriceEur(int priceEur) {
        this.priceEur = priceEur;
    }

    public int getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(int priceUsd) {
        this.priceUsd = priceUsd;
    }

    public List<OrderItemDto> getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(List<OrderItemDto> orderedItems) {
        this.orderedItems = orderedItems;
    }
}
