package cz.o2.eshop.dto;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 11.1.2018.
 */
public class OrderItemDto {

    private int id;
    private int quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
