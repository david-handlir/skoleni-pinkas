package cz.o2.eshop.repository;

import cz.o2.eshop.entity.Item;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 9.1.2018.
 */

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

    // Named query je asi best practice
    @Query("select i from Item i where i.id = ?1")
    Item findOneIten(int id);

    // transactional (problem muze byt s postgres kde vyvolava autocommit)
    List<Item> findByName(String name, Pageable pageable);

    @Query(nativeQuery = true, value = "select coumt(*) from item")
    long countItems();

    @Query("select i from Item i left join fetch i.orderedItems")
    List<Item> findAllFetch(Sort sort);

}
