package cz.o2.eshop.service;

import cz.o2.eshop.dto.ItemDto;
import cz.o2.eshop.entity.Item;
import cz.o2.eshop.exception.EntityNotFoundException;
import cz.o2.eshop.repository.ItemRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 9.1.2018.
 */

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private MapperFacade mapperFacade;

//    //Soucast AOP - aspekt pro cachovani, problem je ze funkce aby se cachovala musi byt volana pres proxy.
//    //Nemuzu cachovat nejakou funkci co bude pouzita jako inner. Cacheable je mozne definovat akorat na public metodach
    @Cacheable("items")
    @Transactional
    public List<ItemDto> findAll(String direction, String column) {
//        //vytvari pro kazdy radek dalsi select
//        List<Item> items = itemRepository.findAll(new Sort(Sort.Direction.fromString(direction.toUpperCase()),column));
        List<Item> items = itemRepository.findAllFetch(new Sort(Sort.Direction.fromString(direction.toUpperCase()),column));
        List<ItemDto> itemDtos = mapperFacade.mapAsList(items, ItemDto.class);
        return itemDtos.stream()
                .map(mapPrices)
                .collect(Collectors.toList());
    }

//    @Scheduled(fixedDelay = 10_000) // automaticky refresh cache
    @CacheEvict(cacheNames = "items", allEntries = true)
    public void clearCache() {
        System.out.println("cache cleared ...");
    }

    public ItemDto findOne(int id) {
        Item item = itemRepository.findOne(id);
        if (item == null) {
            throw new EntityNotFoundException(String.format("Entity with id: %d not found!", id));
        }
        ItemDto itemDto = mapperFacade.map(item, ItemDto.class);
        itemDto.setPriceEur(item.getPrice() / 25);
        itemDto.setPriceUsd(item.getPrice() / 20);
        return itemDto;
    }

    private Function<ItemDto, ItemDto> mapPrices = (itemDto) -> {
        itemDto.setPriceEur(itemDto.getPrice() / 25);
        itemDto.setPriceUsd(itemDto.getPrice() / 22);
        return itemDto;
    };

    public ItemDto save(ItemDto itemDto) {
        Item item = mapperFacade.map(itemDto, Item.class);
        Item savedItem = itemRepository.save(item);
        return mapperFacade.map(savedItem, ItemDto.class);
    }

    public void delete(int id) {
        itemRepository.delete(id);
    }

    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public void setMapperFacade(MapperFacade mapperFacade) {
        this.mapperFacade = mapperFacade;
    }
}
