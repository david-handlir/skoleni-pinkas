package cz.o2.eshop.exception;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 10.1.2018.
 */
//Jedno z moznych reseni, ale my si specifikujeme vice viz. cz.o2.eshop.controller.CustomRestControllerAdvice.handleItemNotFound
//@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}
