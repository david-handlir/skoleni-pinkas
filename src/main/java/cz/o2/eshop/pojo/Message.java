package cz.o2.eshop.pojo;

/**
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 9.1.2018.
 */

//@XmlRootElement //umoznuje defaultne vracet xml
public class Message {

    private String message;

    public Message(String message) {
        this.message = message;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
