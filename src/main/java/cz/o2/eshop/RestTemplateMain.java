package cz.o2.eshop;

import cz.o2.eshop.pojo.Message;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate - uzitecna trida pro praci s Rest architekturou
 *
 * @author <a href="mailto:david.handlir@o2.cz">David Handlir</a> on 10.1.2018.
 */
public class RestTemplateMain {

    public static void main(String[] args) {
//       RestTemplate dobra trida pro praci s rest api
        RestTemplate restTemplate = new RestTemplate();
        String string = restTemplate.getForObject("http://localhost:8080/hello", String.class);
        System.out.println(string);

        Message message = restTemplate.getForObject("http://localhost:8080/hello", Message.class);
        System.out.println(message.getMessage());
    }
}
